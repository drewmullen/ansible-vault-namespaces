# Contributing to this repository

Contributions are welcome to be made to this, especially if there comes a new need for a new `auth` or `secrets` engine that needs to be mounted to a Vault instance. The process to do this is as follows:
1. Create a fork of this repo
2. In said fork, work on new feature
3. Once done, open a PR to the upstream [repository](https://gitlab.com/drewmullen/ansible-vault-namespaces), making sure that the PR description covers the following:
    - What is being added
    - The purpose of this addition
4. Ping a maintainer of the repository to review the PR and to get it merged in

## Guides to Contribute
With how the repository is structure, care is needed to understand how the project is overlayed in order to make sure your new ansible `tasks` are properly executed.

### External Resources
This repo used the `ansible-hashivault-modules`. Links to both the docs and repo can be found below:
- [repo](https://github.com/TerryHowe/ansible-modules-hashivault)
- [docs](https://terryhowe.github.io/ansible-modules-hashivault/modules/list_of_hashivault_modules.html)

### Adding new tasks
To add new tasks is as simple as going into the `/roles/namespace/tasks` directory and either creating a new `yaml` or adding onto an existing `yaml`, depending on the new task you are adding. In general, the following `yamls` are what you will most likely be contributing towards: 
- `auth.yaml`: Mounts a Vault auth engine, calling `auth_role.yaml` in the process.
  - `auth_role.yaml`: Configures a role for the specified auth engine.
- `engine.yaml`: Mounts a Vault secret engine, calling `engine_role.yaml` in the process.
  - `engine_role.yaml`: Configures a role for the specified secret engine.

#### Auths
When contributing a new auth config, in order to interpolate parameters into the methods, the following line is used:
```
- name: "include {{ auth.auth_name }} variables for {{ auth_config }}"
  include_vars: "{{ auths_dir }}/{{ auth.auth_type }}/config/{{ auth_config }}.json
```
As such, all method calls should lie past this, since Ansible executes `tasks` linearly. This also requires a new `json` file to be declared in the respected folder of the auth endpoint, i.e. for Kubernetes, `auths/kubernetes/config/example_config_name.json`. This same style is also applied to auth roles, with the only difference being the directory used, i.e. for Kubernetes, `auths/kubernetes/roles/example_role_name.json`
> The name of the `json` file is signifigant, so keep in mind of this name for later.

Once the tasks and jsons are set up, to add the new auth into the playbook, simply add it to the `namespace_config.yaml` that you will be using when invoking this repo's playbook:
```
root:
    auths:
      - auth_type: <name_of_auth_type> (kubernetes)
        auth_name: <endpoint_name_for_auth> (can be the same as auth_type)
        mount_config:
          (Adds additional config to the auth)
        auth_config: <name_of_json_file> (file that is located in auths/auth_name/config/)
        roles: <name_of_json_file> (file(s) that is(are) located in auths/auth_name/roles/)
```
For sake of consistency + reference point, be sure to template the new jsons in `/configs/auths/`, as well as updating its usage in the root `README.md`.

### Adding to Molecule
`molecule` provides a way to validate the playbook declared in here without needing a preexisting environment spun up to deploy the playbook on. The filestructure in there is similar to the filestruture seen in `roles/namespaces` with a few exceptions:
- All values will be passed into it through `molecule/default/configs`, so make sure to add in all `json` files that are used in their proper places there
- `config_test.yaml` is where all of the auths/secret engines are mounted, so make sure to specify new tasks in there akin to the earlier step.